# Configure-PhishThreatTenant



## Purpose

Configures a 365 tenant for compatability with Sophos Phish Threat so that phishing campaigns don't get triggered by 365's email security measures.
