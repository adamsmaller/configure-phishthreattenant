Connect-ExchangeOnline -UserPrincipalName "<global_admin_addr>"
Connect-IPPSSession 

Set-HostedContentFilterPolicy -Id Default -AllowedSenderDomains 
    @{Add="auditmessages.com","bankfraudalerts.com","buildingmgmt.info","corporate-realty.co","court-notices.com",
    "e-billinvoices.com","e-documentsign.com","e-faxsent.com","e-receipts.co","epromodeals.com","fakebookalerts.live",
    "global-hr-staff.com","gmailmsg.com","helpdesk-tech.com","hr-benefits.site","it-supportdesk.com","linkedn.co",
    "mail-sender.online","myhr-portal.site","online-statements.site","outlook-mailer.com","secure-bank-alerts.com",
    "shipping-updates.com","tax-official.com","toll-citations.com","trackshipping.online","voicemailbox.online","awstrack.me"}

## CHECK FOR POLICY FIRST
if (!(Get-PhishSimOverridePolicy)) {
    New-PhishSimOverridePolicy -Name "PhishSimOverridePolicy" -Enabled $true
}

New-PhishSimOverrideRule -Name "Sophos Phish Threat" -Policy PhishSimOverridePolicy -Domains "auditmessages.com","bankfraudalerts.com",
    "buildingmgmt.info","corporate-realty.co","court-notices.com","e-billinvoices.com","e-documentsign.com","e-faxsent.com",
    "e-receipts.co","epromodeals.com","fakebookalerts.live","global-hr-staff.com","gmailmsg.com","helpdesk-tech.com",
    "hr-benefits.site","it-supportdesk.com","linkedn.co","mail-sender.online","myhr-portal.site","online-statements.site" 
    -SenderIpRanges 54.240.51.52,54.240.51.53

New-TransportRule -Name "Sophos Phish Threat - Bypass Safe Links" -SetHeaderName "X-MS-Exchange-Organization-SkipSafeLinksProcessing" 
    -SetHeaderValue "1" -SenderDomainIs "auditmessages.com","bankfraudalerts.com","buildingmgmt.info","corporate-realty.co","court-notices.com",
    "e-billinvoices.com","e-documentsign.com","e-faxsent.com","e-receipts.co","epromodeals.com","fakebookalerts.live","global-hr-staff.com",
    "gmailmsg.com","helpdesk-tech.com","hr-benefits.site","it-supportdesk.com","linkedn.co","mail-sender.online","myhr-portal.site",
    "online-statements.site","outlook-mailer.com","secure-bank-alerts.com","shipping-updates.com","tax-official.com","toll-citations.com",
    "trackshipping.online","voicemailbox.online","awstrack.me","sophos-phish-threat.go-vip.co","go-vip.co"

New-TransportRule -Name "Sophos Phish Threat - Bypass Safe Attachments" -SetHeaderName "X-MS-Exchange-Organization-SkipSafeAttachmentProcessing" 
    -SetHeaderValue "1" -SenderDomainIs "auditmessages.com","bankfraudalerts.com","buildingmgmt.info","corporate-realty.co","court-notices.com",
    "e-billinvoices.com","e-documentsign.com","e-faxsent.com","e-receipts.co","epromodeals.com","fakebookalerts.live","global-hr-staff.com",
    "gmailmsg.com","helpdesk-tech.com","hr-benefits.site","it-supportdesk.com","linkedn.co","mail-sender.online","myhr-portal.site",
    "online-statements.site","outlook-mailer.com","secure-bank-alerts.com","shipping-updates.com","tax-official.com","toll-citations.com",
    "trackshipping.online","voicemailbox.online","awstrack.me","sophos-phish-threat.go-vip.co","go-vip.co"